#!/usr/bin/python3

import os
import csv
import time
import subprocess



# get folder location
def main():
    dir_path = os.getcwd()

    newDataArray = []

    # print("   The current path is: \n" + dir_path + "\n   Please make sure your file named:\n")
    # print(" packagelist.txt \n")
    # print("   is in this directory\n")
    # print("\n\n")
    answer = input("Ready to Run? [Y/n]: ")
    if answer == "Y":




        # run code here to list row
        # create the packagelist file using pacman -Q > packagelist.txt

        subprocess.call("pacman -Qqen > packagelist-pacman.txt", shell=True)
        

        #need separate by space then print first row to new text file
        with open('packagelist-pacman.txt', newline='') as f:
            reader = csv.reader(f,  delimiter=' ')
            for row in reader:
               # print was just for test
               # print(row[0])
                newDataArray.append(row[0])
        # test array
        # print (newDataArray)
        # write array to txt
        output = csv.writer(open('packageList-pacman.txt', 'w'), delimiter=' ', lineterminator='\n')
        for x in newDataArray: output.writerow([x])

        # write array to csv
        output2 = csv.writer(open('packageList-pacman.csv', 'w'), delimiter=',', lineterminator='\n')
        for x in newDataArray: output2.writerow([x])            

        print("packagelist-pacman.txt created in directory: " + dir_path + "\n")
        print("packagelist-pacman.csv created in directory: " + dir_path + "\n")    

        # run code here to list row
        # create the packagelist file using pacman -Q > packagelist.txt

        subprocess.call("pacman -Qqem > packagelistyay.txt", shell=True)
        

        #need separate by space then print first row to new text file
        with open('packagelistyay.txt', newline='') as f:
            reader = csv.reader(f,  delimiter=' ')
            for row in reader:
               # print was just for test
               # print(row[0])
                newDataArray.append(row[0])
        # test array
        # print (newDataArray)
        # write array to txt
        output = csv.writer(open('packageListyay.txt', 'w'), delimiter=' ', lineterminator='\n')
        for x in newDataArray: output.writerow([x])

        # write array to csv
        output2 = csv.writer(open('packageListyay.csv', 'w'), delimiter=',', lineterminator='\n')
        for x in newDataArray: output2.writerow([x])            

        print("packagelistyay.txt created in directory: " + dir_path + "\n")
        print("packagelistyay.csv created in directory: " + dir_path + "\n")    



    else:
        print("You have chosen to exit by not saying Y\n")
        print("Exiting in 5 seconds . . .\n")
        time.sleep(5)
        exit()

if __name__ == "__main__":

    main()
